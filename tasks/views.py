from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid:
            task = form.save()
            task.purchaser = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create-task-view.html", context)


@login_required
def task_list_view(request):
    alltasks = Task.objects.filter(assignee=request.user)
    context = {"alltasks": alltasks}
    return render(request, "tasks/tasks-list-view.html", context)
